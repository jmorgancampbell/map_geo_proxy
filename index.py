from bottle import route, run, HTTPResponse
import requests


@route('/<z>/<x>/<y>', method='GET')
def index(z, x, y):
    url = 'https://wmts10.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-farbe/default/current/3857/{}/{}/{}.jpeg'.format(z, x, y)
    print url
    r = requests.get(
        url,
        headers={'referer': 'https://map.geo.admin.ch'},
        stream=True
    )
    resp = HTTPResponse(body=r.content)
    resp.set_header('content_type', 'image/jpeg')
    return resp


@route('/', method='GET')
def check():
    return 'check'


run(host='localhost', port=8080)